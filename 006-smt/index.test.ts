import { elasticize } from ".";


// Even length
it("Test with even length word.", () => {
    expect(elasticize("ANNA")).toBe("ANNNNA");
});

it("Test with even length word.", () => {
    expect(elasticize("BANANA")).toBe("BAANNNAAANNA");
});


// Odd length
it("Test with odd length word.", () => {
    expect(elasticize("KAYAK")).toBe("KAAYYYAAK");
});

it("Test with even length word.", () => {
    expect(elasticize("XCX")).toBe("XCCX");
});


// Too short
it("Too short word, must return the same word", () => {
    expect(elasticize("XX")).toBe("XX");
});

it("Too short word, must return the same word", () => {
    expect(elasticize("X")).toBe("X");
});

it("Too short word, must return the same word", () => {
    expect(elasticize("")).toBe("");
});