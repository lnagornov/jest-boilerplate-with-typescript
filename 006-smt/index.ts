/**
* Returns the elasticized version of the word as a string. 
*
* @param  {string} word - Word to elasticize.
* @returns {string}
*/
export function elasticize(word) {
    const length = word.length;
    if (length < 3) {
        return word;
    }

    let isEvenLength = length % 2 === 0;
    let centerIndex = Math.floor(length / 2);
    let elasticizeChars = [];

    // left side
    for (let i = 0; i < centerIndex; i++) {
        elasticizeChars.push(word.at(i).repeat(i+1));
    }
    // center
    if (!isEvenLength){
        elasticizeChars.push(word.at(centerIndex).repeat(centerIndex+1));
    }
    // right side
    for (let i = isEvenLength ? centerIndex: centerIndex + 1; i < length; i++) {
        elasticizeChars.push(word.at(i).repeat(length-i));
    }
    
    return elasticizeChars.join("");
}