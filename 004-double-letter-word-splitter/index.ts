/**
 * Splits the text where any double letter is found and returns an array of the split words.
 * If no repeated letters are found, returns an empty array.
 * 
 * @param  {string} rawText - Text to take search in.
 * @returns {Array<string>}
 */
export function splitOnDoubleLetter(rawText) {
    const text = rawText.toLowerCase();
    const tokens = [];
    let lastIndex = 0;
    for (let i = 0; i < text.length - 1; i++) {
        if (text[i] === text[i + 1]) {
            tokens.push(text.slice(lastIndex, i + 1));
            lastIndex = i + 1;
        }
    }
    if (tokens.length > 0) {
        tokens.push(text.slice(lastIndex));
    }
    
    return tokens;
}