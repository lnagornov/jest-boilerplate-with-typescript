import { splitOnDoubleLetter } from "../004-double-letter-word-splitter";


it("Shout split, because there is double T in the word", () => {
    expect(splitOnDoubleLetter("Letter")).toEqual(["let", "ter"]);
});

it("Shout split and return array of lowercase strings.", () => {
    expect(splitOnDoubleLetter("FALL")).toEqual(["fal", "l"]);
});

it("Shout split, because there is double L in the word", () => {
    expect(splitOnDoubleLetter("Really")).toEqual(["real", "ly"]);
});

it("Shout split, because there is double P in the word", () => {
    expect(splitOnDoubleLetter("Happy")).toEqual(["hap", "py"]);
});

it("Shout split, because there is double L in the word", () => {
    expect(splitOnDoubleLetter("Shall")).toEqual(["shal", "l"]);
});

it("Shout split, because there is double O in the word", () => {
    expect(splitOnDoubleLetter("Tool")).toEqual(["to", "ol"]);
});

it("Shout split, because there are 3 double letters S S P in the word", () => {
    expect(splitOnDoubleLetter("Mississippi")).toEqual(
        ["mis", "sis", "sip", "pi"]
    );
});

it("Shout not split word, no double letters.", () => {
    expect(splitOnDoubleLetter("Easy")).toEqual([]);
});
