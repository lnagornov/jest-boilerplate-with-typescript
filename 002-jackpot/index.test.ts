import {isJackpot} from ".";


// Positive testing
it("Should return true as all elements are the same string", () => {
    expect(isJackpot(["@", "@", "@", "@"])).toBe(true);
});

it("Should return true as all elements are the same string", () => {
    expect(isJackpot(["abc", "abc", "abc", "abc"])).toBe(true);
});

it("Should return true as all elements are the same string", () => {
    expect(isJackpot(["1", "1", "1", "1"])).toBe(true);
});

it("Should return true as all elements are the same with type <undefined>", () => {
    expect(isJackpot([undefined, undefined, undefined, undefined])).toBe(true);
});

it("Should return true as all elements are the same with type <int>", () => {
    expect(isJackpot([2, 2, 2, 2])).toBe(true);
});

it("Should return true as all elements are the same with type <null>", () => {
    expect(isJackpot([null, null, null, null])).toBe(true);
});


// Negative testing
it("Should return false as not all elements are the same (1 different <string> element)", () => {
    expect(isJackpot(["a", "@", "@", "@"])).toBe(false);
});

it("Should return false as not all elements are the same (2 different <string> elements)", () => {
    expect(isJackpot(["SS", "Ss", "sS", "SS"])).toBe(false);
});

it("Should return false as not all elements are the same (1 different element with <int> type)", () => {
    expect(isJackpot([1, "1", "1", "1"])).toBe(false);
});

it("Should return false as not all elements are the same (1 different element with <NaN> type)", () => {
    expect(isJackpot([NaN, "NaN", "NaN", "NaN"])).toBe(false);
});

it("Should return false as not all elements are the same (<NaN> elements are not comaprable)", () => {
    expect(isJackpot([NaN, NaN, NaN, NaN])).toBe(false);
});
