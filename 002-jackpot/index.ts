/**
 * This function takes in an array (slot machine outcome)
 * and returns <true>  if all elements in the array are identical
 * and <false> otherwise.
 * 
 * @param args 
 * @returns bool
 */
export function isJackpot(args) {
    return args.every(x => x === args[0]);
}