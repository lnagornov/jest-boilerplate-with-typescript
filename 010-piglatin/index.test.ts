import { pigLatinSentence } from ".";


it("Test 1", () => {
    expect(
        pigLatinSentence("this is pig latin")).toBe("isthay isway igpay atinlay");
});

it("Test 2", () => {
    expect(
        pigLatinSentence("wall street journal")).toBe("allway eetstray ournaljay");
});

it("Test 3", () => {
    expect(
        pigLatinSentence("raise the bridge")).toBe("aiseray ethay idgebray");
});

it("Test 4", () => {
    expect(
        pigLatinSentence("all pigs oink")).toBe("allway igspay oinkway");
});