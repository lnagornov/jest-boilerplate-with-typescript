/**
* Converts a sentence into a simplified pig latin. 
* Rules for converting to pig latin: 
*     For words that begin with a vowel (a, e, i, o, u), add "way" to the end of the word. 
*     Otherwise, move all letters before the first vowel to the end and add "ay". 
*
* @param  {string} text - Text to convert.
* @returns {string}
*/
export function pigLatinSentence(text) {
    const words = text.split(" ");

    const isVowel = (char) => {
        return ["a", "e", "i", "o", "u"].includes(char.toLowerCase());
    };

    const moveConsonants = (word) => {
        for (let i = 0; i < word.length; i++) {
            if (isVowel(word[i])) {
                return word.slice(i, word.length) + word.slice(0, i);
            }
        }
    };

    return words.map(word => isVowel(word[0]) ? word + "way" : moveConsonants(word) + "ay")
                .join(" ");
}