/**
* Gets a sentence with numbers representing a word's location 
* embedded within each word and returns the sorted sentence.
*
* @param  {string} text - Text to rearrange.
* @returns {string}
*/
export function rearrange(text) {
    let words = text.trim().split(" ");
    let rearrangedSentence = new Array(words.length);

    for (const word of words) {
        for (const char of word.split("")) {
            if (!isNaN(char)) {
                let index = parseInt(char);
                rearrangedSentence[index - 1] = word.replace(/[^a-zA-Z]/g, "");
            }
        }
    }
    
    return rearrangedSentence.join(" ");
}