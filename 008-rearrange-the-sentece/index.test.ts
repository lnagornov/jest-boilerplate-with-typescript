import { rearrange } from ".";


it("Test 1", () => {
    expect(
        rearrange("is2 Thi1s T4est 3a")).toBe("This is a Test");
});

it("Test 2", () => {
    expect(
        rearrange("4of Fo1r pe6ople g3ood th5e the2")).toBe("For the good of the people");
});

it("Test 3", () => {
    expect(
        rearrange("5weird i2s JavaScri1pt dam4n so3")).toBe("JavaScript is so damn weird");
});

it("Test 4", () => {
    expect(
        rearrange(" ")).toBe("");
});