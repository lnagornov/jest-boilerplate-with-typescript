import { noStrangers } from ".";


it("Test 1", () => {
    expect(
        noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.")
    ).toEqual([["spot", "see"], []]);
});

it("Test 2", () => {
    expect(
        noStrangers("Don't you see that's /comming^ don't don't? that's that's that's that's")
    ).toEqual([["don't", "that's"], ["that's"]]);
});