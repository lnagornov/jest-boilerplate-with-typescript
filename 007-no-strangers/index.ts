/**
* Takes the string and returns an array of two arrays.
*
* A word encountered for the first time is a stranger.  
* A word encountered thrice becomes an acquaintance.  
* A word encountered 5 times becomes a friend.
*
* The first is an array of acquaintances in the order they became an acquaintance (see 
* example).  
*
* The second is an array of friends in the order they became a friend. Words in the friend 
* array should no longer be in the acquaintance array.
*
* @param  {string} rawText - Text to analyze.
* @returns {Array<string>}
*/
export function noStrangers(text) {
    let words = text.trim().toLowerCase().replace(/[^\w\s']|_/g, "").replace(/\s+/g, " ").split(" ");
    let counter = {};
    let acquaintances = [];
    let friends = [];

    const checkFriendship = (word) => {
        let repeats = counter[word];
        switch(repeats) {
            case 3:
                acquaintances.push(word);
                break;
            case 5:
                friends.push(word);
                break;
        }
    };

    for (const word of words) {
        if (counter.hasOwnProperty(word)) {
            counter[word] += 1;
            checkFriendship(word);
        } else {
            counter[word] = 1;
        }
    }

    return [acquaintances, friends];
}