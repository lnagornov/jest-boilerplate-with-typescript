/**
 * Returns number of times the first string (the single character)
 * is found in the second string.
 * @param {string} char - Seeking character.
 * @param  {string} text - Text to look search in.
 * @returns {number}
 */
export function charCount(char, text) {
    return text.split(char).length - 1;
}