import { splitOnDoubleLetter } from "../004-double-letter-word-splitter";


it("", () => {
    expect(splitOnDoubleLetter("Letter")).toBe(["let", "ter"]);
});

it("", () => {
    expect(splitOnDoubleLetter("Really")).toBe(["real", "ly"]);
});

it("", () => {
    expect(splitOnDoubleLetter("Happy")).toBe(["hap", "py"]);
});

it("", () => {
    expect(splitOnDoubleLetter("Shall")).toBe(["shal", "l"]);
});

it("", () => {
    expect(splitOnDoubleLetter("Tool")).toBe(["to", "ol"]);
});

it("", () => {
    expect(splitOnDoubleLetter("Mississippi")).toBe(
        ["mis", "sis", "sip", "pi"]
    );
});

it("", () => {
    expect(splitOnDoubleLetter("Easy")).toBe([]);
});