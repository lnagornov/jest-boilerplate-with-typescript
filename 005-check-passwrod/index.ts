/**
* Checks the strength of passed password.
* There are 5 criteria which determine the strength of a password. 
* (Nordic letters are not catered for simplicity.) 
*
* A password is said to be strong if it satisfies the following criteria: 
* 1.  It contains at least one lowercase English (non-Nordic) character. 
* 2.  It contains at least one uppercase English (non-Nordic) character. 
* 3.  It contains at least one special character. You can simply check for not alphanumeric.  
* 4.  Its length is at least 8. 
* 5.  It contains at least one digit
*
* Moderate or Weak? 
* 1.  If the password matches 3-4 of the above criteria it is moderate and  
* 2.  If it matches only 1-2 it is weak. 
* 3.  If the password is less than 6 characters long, or contains white spaces, return invalid.
* 
* @param  {string} password - Password to check strength.
* @returns {string}
*/
export function checkPassword(password) {
    const hasLengthLess6 = password.length < 6;
    const hasWhiteSpace = /[ ]/.test(password);
    if (hasLengthLess6 || hasWhiteSpace) {
        return "Invalid";
    }

    const hasOneLowercaseLetter = /[a-z]/.test(password);
    const hasOneUppercaseLetter = /[A-Z]/.test(password);
    const hasOneSpecialCharacter = /[^a-zA-Z0-9]/.test(password);
    const hasLengthAtLeast8 = password.length >= 8;
    const hasOneDigit = /[0-9]/.test(password);
    
    let matchedCriteria = [
        hasOneLowercaseLetter,
        hasOneUppercaseLetter,
        hasOneSpecialCharacter,
        hasLengthAtLeast8,
        hasOneDigit
    ]
        .reduce((a, b) => Number(a) + Number(b), 0);
    
    switch (matchedCriteria) {
        case 5:
            return "Strong";
        case 4:
        case 3:
            return "Moderate";
        case 2:
        case 1:
            return "Weak";
        default:
            return "Invalid";
    }
}