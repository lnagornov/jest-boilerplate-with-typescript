import { checkPassword } from ".";


it("Invalid password.", () => {
    expect(checkPassword("stonk")).toBe("Invalid");
});

it("Invalid password.", () => {
    expect(checkPassword("pass word")).toBe("Invalid");
});



it("Weak password.", () => {
    expect(checkPassword("password")).toBe("Weak");
});

it("Weak password.", () => {
    expect(checkPassword("11081992")).toBe("Weak");
});


it("Moderate password.", () => {
    expect(checkPassword("mySecurePass123")).toBe("Moderate");
});

it("Moderate password.", () => {
    expect(checkPassword("!@!pass1")).toBe("Moderate");
});


it("Strong password.", () => {
    expect(checkPassword("@S3cur1ty")).toBe("Strong");
});