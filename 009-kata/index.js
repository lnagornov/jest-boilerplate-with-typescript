/**
* Gets a sentence with numbers representing a word's location 
* embedded within each word and returns the sorted sentence.
*
* @param  {string} text - Text to rearrange.
* @returns {string}
*/
function something(text) {

    const getConsonants = (text) => {
        let reg = text.match(/[^aeiou]/gi);
        return Object.keys(reg).map(key => key.toString());
        // return text.filter(x => !["a", "e", "i", "o", "u"].includes(x));
    };

    const getVowels = (text) => {
        let reg = text.match(/[aeiou]/gi);
        return Object.keys(reg).map(key => key.toString());
        // return text.filter(x => ["a", "e", "i", "o", "u"].includes(x));
    };

    const createSurname = surname => {
        if (surname.length < 3) {
            return surname + "X".repeat(3 - surname.length);
        } 
        
        let vowels = getVowels(surname);
        let consonants = getConsonants(surname);
        console.log(typeof vowels);
        console.log(typeof consonants);
        let consonantsAmount = consonants.length;
        if (consonantsAmount > 2) {
            return consonants.slice(0, 3).join("");
        } else {
            return consonants.push(vowels.slice(0, 3 - consonantsAmount)).join("");
        }
        
    };
    createSurname(text);
    return [];
}

something("Hello");
something("Ayuo");
something("Ay");
something("Ayl");